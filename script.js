const btn = document.querySelector(".button-ip");
urlIp = "https://api.ipify.org/?format=json";
urlLocation = "http://ip-api.com/";

const infoContainer = document.querySelector(".container");
const ul = document.createElement("ul");
infoContainer.appendChild(ul);

btn.addEventListener("click", async function () {
  btn.innerHTML = "Зачекайте...";
  ul.innerHTML = "";
  const ip = await fetch(urlIp);
  const resp = await ip.json();
  const loc = await fetch(
    `${urlLocation}json/${resp.ip}?fields=continent,country,regionName,city,query`
  );
  const locresp = await loc.json();
  const { query, continent, country, regionName, city } = locresp;
  const fineLoc = { IP: query, continent, country, region: regionName, city };

  Object.keys(fineLoc).forEach(function (key) {
    let li = document.createElement("li");
    li.textContent = `${key}: ${fineLoc[key]}`;
    ul.append(li);
  });

  btn.innerHTML = "Спробувати ще раз";
});
